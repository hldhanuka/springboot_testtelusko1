package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class TestTelusko1Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(TestTelusko1Application.class, args);

		Alien a = context.getBean(Alien.class);

		a.show();
	}
}
